#!../../bin/linux-x86_64/fx

## You may have to change fx to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/fx.dbd",0,0)
fx_registerRecordDeviceDriver(pdbbase)

falconXPortConfigure("X1", "10.1.1.49")

## Load record instances
dbLoadRecords("db/falconX.db","P=SR05ID01FX1,PORT=X1")

cd ${TOP}/iocBoot/${IOC}
iocInit()

## Start any sequence programs
#seq sncxxx,"user=rhydera"
