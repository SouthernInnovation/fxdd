#ifndef FXAPP_FALCONX_H
#define FXAPP_FALCONX_H

// Headers.
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

// SITORO includes.
#include "sinc.h"
#include "lmbuf.h"

// FalconX globals
#define EPICS_STRING_LEN 39
#define MAX_PORTS 10
#define MAX_NAME_LEN 80
#define MAX_IP_LEN 80
#define MAX_ADDRESS_LEN 160
#define MAX_CAPTURE_DIR_LEN 1024
#define MAX_PATH_SECTION_LEN 1024
#define MAX_PATH_SECTIONS 4
#define MAX_CAPTURE_FILE_PATTERN_LEN 1024
#define MAX_CHANNELS 32
#define SINC_TIMEOUT 5000
#define PACKET_POLL_PERIOD 100
#define STOP_POLL_PERIOD 10
#define WRITE_BUF_SIZE 256


// List of functions for falconX
// Currently just get and set parameters. Additional could include calibration, restart, start list mode, etc
enum functions
{ 
    RI_FUNCTION_UNDEFINED,
    RI_FUNCTION_GETPARAM,
    RI_FUNCTION_SETPARAM,
    RI_FUNCTION_OSCILLOSCOPE,
    RI_FUNCTION_HISTOGRAM,
    RI_FUNCTION_LISTMODE,
    RI_FUNCTION_RSTSPTLSSTM,
    RI_FUNCTION_STOP,
    RI_FUNCTION_RESTART,
    RI_FUNCTION_CAPTURE,
    RI_FUNCTION_GET_CAPTURE_STATUS,
    RI_FUNCTION_GET_CAPTURE_DIR,
    RI_FUNCTION_SET_CAPTURE_DIR,
    RI_FUNCTION_GET_CAPTURE_FILE_PATTERN,
    RI_FUNCTION_SET_CAPTURE_FILE_PATTERN,
    RI_FUNCTION_GET_CAPTURE_PATH_SECTION_1,
    RI_FUNCTION_SET_CAPTURE_PATH_SECTION_1,
    RI_FUNCTION_GET_CAPTURE_PATH_SECTION_2,
    RI_FUNCTION_SET_CAPTURE_PATH_SECTION_2,
    RI_FUNCTION_GET_CAPTURE_PATH_SECTION_3,
    RI_FUNCTION_SET_CAPTURE_PATH_SECTION_3,
    RI_FUNCTION_GET_CAPTURE_PATH_SECTION_4,
    RI_FUNCTION_SET_CAPTURE_PATH_SECTION_4,
    RI_FUNCTION_GET_MAX_CAPTURE_FILE_SIZE,
    RI_FUNCTION_SET_MAX_CAPTURE_FILE_SIZE,
    RI_FUNCTION_GET_NEXT_RUN_NUMBER,
    RI_FUNCTION_SET_NEXT_RUN_NUMBER,
    RI_GET_PULSE_COUNT_RATE_COMBINED,
    RI_GET_PULSE_COUNT_RATE_INDIVIDUAL,
    RI_FUNCTION_NUM_FUNCTIONS 
};

// Possible capture states:
//    CS_OFF - not capturing.
//    CS_COUNT_RATE - running list mode but not capturing to files. Count rate is available.
//    CS_CAPTURE - capturing list mode data to files.
enum captureState
{
    CS_OFF = 0,
    CS_COUNT_RATE = 1,
    CS_CAPTURE = 2
};


//
// Information we keep on each channel of captured list mode data.
//
typedef struct captureChannel {
    LmBuf  decodeBuf;                           // List mode decoder buffer.
    FILE  *outFile;                             // A file we're writing to.
    size_t writtenBytes;                        // The number of bytes written to the current output file.

    // Count rate accumulation.
    int    accumCount;                          // The accumulated pulse count since the last check.
    time_t accumLastUpdate;                     // When we last updated the count rate.
    int    pulseCountRate;                      // The count rate on this channel, updated periodically.
    pthread_mutex_t pulseCountRateMutex;        // Mutex to prevent simultaneous access to capture variables.
} captureChannel;

// FalconX ports.
// Set up at IOC start.
// Typical line in st.cmd could be
//   falconXPortConfigure("X1","10.114.2.50")
typedef struct falconXport {
    // Port related data.
    char      name[MAX_NAME_LEN+1];             // Name as defined in st.cmd  (X1 in example above)
    char      address[MAX_IP_LEN+1];            // IP address as defined in st.cmd
    int       runNumber;                        // A unique number for a capture run.
    char      captureDir[MAX_CAPTURE_DIR_LEN+1]; // where to capture files to.
    char      captureFilePattern[MAX_CAPTURE_FILE_PATTERN_LEN+1]; // a file pattern for created files.
    char      path[MAX_PATH_SECTIONS][MAX_PATH_SECTION_LEN+1];    // a section of a path to capture files to.
    long      maxCaptureFileSize;               // the maximum capture file size before splitting.
    Sinc      sinc;                             // sinc connection context.
    int       numChannels;                      // The number of channels in this device.
    pthread_mutex_t mutex;                      // Mutex to prevent simultaneous access.

    // List mode data capture.
    enum captureState state;                    // Which capture state the system's in: off/count rate only/capture. 
    pthread_t captureThread;                    // Handle to the capture thread.
    pthread_mutex_t captureMutex;               // Mutex to prevent simultaneous access to capture variables.
    bool      captureThreadRunning;             // true if the capture thread is running.
    Sinc      captureSinc;                      // Sinc connection for capturing data.
    time_t    captureStartTime;                 // Capture started at this time.
    uint32_t  packetFilter;                     // Bitset of packet types to filter out.
    int       splitCount;                       // If we're splitting output to multiple files, which file we're up to.
    captureChannel captureChannels[MAX_CHANNELS]; // Information on each channel we're receiving data from.
} falconXport;


// Used to hold record information for all record types
typedef struct
{
  struct dbCommon* record;                      // EPICS record
  int portIndex;                                // Index into falconXports. -1 if undefined
  int function;                                 // Function from PV address specification. For example, RI_FUNCTION_GETPARAM
  char name[MAX_NAME_LEN];
  char functionName[MAX_NAME_LEN];

  SiToro__Sinc__KeyValue__ParamType paramType;  // Sinc parameter type to read or write from
  bool hasValue;                                // True if a value was obtained when reading. Unused for writing
  union
  {
    float floatValue;                           // Value for reading and writing floating parameters
    bool  binaryValue;                          // Value for reading and writing binary parameters
    int   intValue;                             // Value for reading and writing integer parameters
    char  stringValue[EPICS_STRING_LEN+1];      // Value for reading and writing string parameters
  } v;                                          // Union of data types for reading and writing parameters
} recordInfo;



// Prototypes from fxcapture.c
void fxCaptureStart(falconXport *p, enum captureState newCaptureState);
void fxCaptureStop(falconXport *p);
bool ensureDirectoryExists(char *path);

// Prototypes from config.c
void configRead(falconXport *p);
int  runNumberRead();
void runNumberWrite(int runNumber);
int  runNumberReadAndIncrement();
bool loadDeviceConfig(falconXport *p);

#endif // FXAPP_FALCONX_H
