// $File: //ASP/tec/det/falconX/trunk/fxApp/src/devFalconX.c $
// $Revision: #4 $
// $DateTime: 2017/08/21 11:40:34 $
// Last checked in by: $Author: afsharn $
//
// Description
// This file is a driver for the FalconX detector signal processor.
// This driver supports multiple record types. The address specification determines
// what interaction occurs with the FalconX detector.
//
// For setting and getting parameters, the record type must match the data .
// For example, a bi record with an address specification of $(PORT)/GETPARAM/spatial.axis_1.enable
// will make a SincGetParam() call requesting parameter spatial.axis_1.enable, which is a boolean.
//
// For each record, an associated recordInfo structure is created to manage the record's interaction with the falconX.
//
// NOTE, All records in this driver use the EPICS callbackRequest() function to allow simple blocking calls the FalconX.
//       Use of callbackRequest will serialise all record processing using this function.
//       For a single FalconX this will not have much effect since each request is serialised over the network anyway.
//       It may have some effect if additional preemptive functionality is added.
//       For example, the ability to restart the FalconX, where a restart command may have to wait for
//       an earlier request to timeout before the restart is sent.
//       It may be a bigger issue for multiple FalconX units where commands to different units
//       that could run concurrently are serialised.
//
//
// Copyright (c) 2017 Australian Synchrotron
//
// This driver is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// Licence as published by the Free Software Foundation; either
// version 2.1 of the Licence, or (at your option) any later version.
//
// This driver is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public Licence for more details.
//
// You should have received a copy of the GNU Lesser General Public
// Licence along with this mallard.ary; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
//
// Contact details:
// andrew.rhyder@synchrotron.org.au
// 800 Blackburn Road, Clayton, Victoria 3168, Australia.
//


// System includes
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

// EPICS includes
#include "alarm.h"
#include "cvtTable.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "recGbl.h"
#include "recSup.h"
#include "devSup.h"

#include "aiRecord.h"
#include "aoRecord.h"

#include "biRecord.h"
#include "boRecord.h"

#include "mbbiRecord.h"
#include "mbboRecord.h"

#include "longinRecord.h"
#include "longoutRecord.h"

#include "stringinRecord.h"
#include "stringoutRecord.h"

#include "link.h"
#include "epicsExport.h"
#include "callback.h"
#include "iocsh.h"

#include "falconx.h"


// Global channel id.
int           channelId = 0;

// Device ports.
falconXport falconXports[MAX_PORTS];            // List of ports
static int portCount=0;                         // Number of ports defined in falconXports. (remaining ports are undefined)


/* dset for ai record*/
static long init_airecord();
static long read_ai();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        read;
        DEVSUPFUN        special_linconv;
} devAifalconX = {
        6,
        NULL,
        NULL,
        init_airecord,
        NULL,
        read_ai,
        NULL
};
epicsExportAddress( dset,devAifalconX );

/* dset for bi record */
static long init_birecord();
static long read_bi();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        read;
        DEVSUPFUN        special_linconv;
} devBifalconX = {
        6,
        NULL,
        NULL,
        init_birecord,
        NULL,
        read_bi,
        NULL
};
epicsExportAddress( dset,devBifalconX );

/* dset for mbbi record*/
static long init_mbbirecord();
static long read_mbbi();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        read;
        DEVSUPFUN        special_linconv;
} devMbbifalconX = {
        6,
        NULL,
        NULL,
        init_mbbirecord,
        NULL,
        read_mbbi,
        NULL
};
epicsExportAddress( dset,devMbbifalconX );

/* dset for stringin record*/
static long init_stringinrecord();
static long read_stringin();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        read;
        DEVSUPFUN        special_linconv;
} devStringinfalconX = {
        6,
        NULL,
        NULL,
        init_stringinrecord,
        NULL,
        read_stringin,
        NULL
};
epicsExportAddress( dset,devStringinfalconX );

/* dset for longin record*/
static long init_longinrecord();
static long read_longin();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        read;
        DEVSUPFUN        special_linconv;
} devLonginfalconX = {
        6,
        NULL,
        NULL,
        init_longinrecord,
        NULL,
        read_longin,
        NULL
};
epicsExportAddress( dset,devLonginfalconX );

/* dset for ao record*/
static long init_aorecord();
static long write_ao();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        write;
        DEVSUPFUN        special_linconv;
} devAofalconX = {
        6,
        NULL,
        NULL,
        init_aorecord,
        NULL,
        write_ao,
        NULL
};
epicsExportAddress( dset,devAofalconX );

/* dset for bo record*/
static long init_borecord();
static long write_bo();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        write;
        DEVSUPFUN        special_linconv;
} devBofalconX = {
        6,
        NULL,
        NULL,
        init_borecord,
        NULL,
        write_bo,
        NULL
};
epicsExportAddress( dset,devBofalconX );

/* dset for mbbo record*/
static long init_mbborecord();
static long write_mbbo();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        write;
        DEVSUPFUN        special_linconv;
} devMbbofalconX = {
        6,
        NULL,
        NULL,
        init_mbborecord,
        NULL,
        write_mbbo,
        NULL
};
epicsExportAddress( dset,devMbbofalconX );

/* dset for mbbo record*/
static long init_stringoutrecord();
static long write_stringout();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        write;
        DEVSUPFUN        special_linconv;
} devStringoutfalconX = {
        6,
        NULL,
        NULL,
        init_stringoutrecord,
        NULL,
        write_stringout,
        NULL
};
epicsExportAddress( dset,devStringoutfalconX );

/* dset for longout record*/
static long init_longoutrecord();
static long write_longout();
struct {
        long             number;
        DEVSUPFUN        report;
        DEVSUPFUN        init;
        DEVSUPFUN        init_record;
        DEVSUPFUN        get_ioint_info;
        DEVSUPFUN        write;
        DEVSUPFUN        special_linconv;
} devLongoutfalconX = {
        6,
        NULL,
        NULL,
        init_longoutrecord,
        NULL,
        write_longout,
        NULL
};
epicsExportAddress( dset,devLongoutfalconX );


// Sinc protocol locking.
void lockSinc(falconXport *p)
{
    pthread_mutex_lock(&p->mutex);
}

void unlockSinc(falconXport *p)
{
    pthread_mutex_unlock(&p->mutex);
}

// Return the port information for a record.
// Port information is held in a list of falconXport structures.
falconXport* getPort( recordInfo* ri )
{
    // Log an error if no port is defined
    if( ri->portIndex < 0 )
    {
      printf( "No port obtained for %s\n", ri->record->name );
      return NULL;
    }

    // Get the port details
    falconXport* p = &falconXports[ri->portIndex];
    if( p == NULL )
    {
      printf( "Port not setup for %s\n", ri->record->name );
      return NULL;
    }

    // Log an error if the port is not connected
    if( !p->sinc.connected )
    {
      printf( "Port not connected for %s, (port: '%s', address: '%s')\n", ri->record->name, p->name, ri->name );
      return NULL;
    }

    // All OK. Return the port
    return p;
}

// Set a parameter on the FalconX
// Called when processing a write function for a record.
void setParam( falconXport* p, recordInfo* ri )
{
    // Initialise the key/value
    SiToro__Sinc__KeyValue kv;
    si_toro__sinc__key_value__init( &kv );
    kv.key = ri->name;

    // set up the key/value according to the data type
    switch( ri->paramType )
    {
        case SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__INT_TYPE:
          kv.has_intval = true;
          kv.intval = ri->v.intValue;
          break;

        case SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__FLOAT_TYPE:
          kv.has_floatval = true;
          kv.floatval = ri->v.floatValue;
          break;

        case SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__BOOL_TYPE:
          kv.has_boolval = true;
          kv.boolval = ri->v.binaryValue;
          break;

        case SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__STRING_TYPE:
          kv.strval = ri->v.stringValue;
          break;

        case SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__OPTION_TYPE:
          kv.optionval = ri->v.stringValue;
          break;

        default:
          printf( "Missing Sinc Key Value Parameter Type (%d) in setParam() for %s\n", ri->paramType, ri->record->name );
          break;
    }

    // Set the parameter
    Sinc* sinc = &p->sinc;
    lockSinc(p);
    bool result = SincSetParam( sinc, channelId, &kv );

    // Log any errors
    if( result == 0 )
    {
        printf( "SincSetParam failed for %s for param: %s error: %d (%s)\n",
                ri->record->name,
                ri->name,
                SincCurrentErrorCode( sinc),
                SincCurrentErrorMessage( sinc) );
    }

    unlockSinc(p);
}

// Get a parameter on the FalconX.
// Called when processing a read function for a record.
void getParam( falconXport* p, recordInfo* ri )
{
    // Get the parameter
    Sinc* sinc = &p->sinc;
    SiToro__Sinc__GetParamResponse* resp = NULL;
    int fromChannelId;
    lockSinc(p);
    bool result = SincGetParam( sinc, channelId, ri->name, &resp, &fromChannelId );

    // Log any errors
    if( result == 0 )
    {
        printf( "SincGetParam failed for %s for param: %s error: %d (%s)\n",
              ri->record->name,
              ri->name,
              SincCurrentErrorCode( sinc ),
              SincCurrentErrorMessage( sinc ) );
        unlockSinc(p);
        return;
    }

    unlockSinc(p);

    if( resp == NULL )
    {
        printf( "SincGetParam response for %s is NULL\n", ri->record->name );
        return;
    }

    if( resp->n_results == 0 )
    {
        printf( "SincGetParam response for %s contains no values\n", ri->record->name );
        si_toro__sinc__get_param_response__free_unpacked( resp, NULL );
        return;
    }

    // Get the first (and only) response
    SiToro__Sinc__KeyValue* kv = resp->results[0];

    // Assume extraction of value will fail
    ri->hasValue = false;

    // Extract the value and flag we have a value.
    // This ignores the PV data type. The PV must be set up to match the parameter.
    // For example, if asking for parameter 'afe.sampleRate' which returns an integer,
    // then the PV will need to be longin which is the only read function which looks for an integer.
    // There is no automatic conversion.
    if( kv->has_intval )
    {
        ri->hasValue = true;
        ri->v.intValue = kv->intval;
    }
    else if( kv->has_floatval )
    {
        ri->hasValue = true;
        ri->v.floatValue = kv->floatval;
    }
    else if( kv->has_boolval )
    {
        ri->hasValue = true;
        ri->v.binaryValue = kv->boolval;
    }
    else if( kv->optionval )
    {
        ri->hasValue = true;
        strncpy( ri->v.stringValue, kv->optionval, EPICS_STRING_LEN );
        ri->v.stringValue[EPICS_STRING_LEN] = 0;
    }
    else if( kv->strval )
    {
        ri->hasValue = true;
        strncpy( ri->v.stringValue, kv->strval, EPICS_STRING_LEN );
        ri->v.stringValue[EPICS_STRING_LEN] = 0;
    }

    // Free the response.
    si_toro__sinc__get_param_response__free_unpacked( resp, NULL );
}

// Get the capture directory.
void getCaptureDir( falconXport* p, recordInfo* ri )
{
    lockSinc(p);
    ri->hasValue = true;
    strncpy( ri->v.stringValue, p->captureDir, EPICS_STRING_LEN );
    ri->v.stringValue[EPICS_STRING_LEN] = 0;
    unlockSinc(p);
}

// Set the capture directory.
void setCaptureDir( falconXport* p, recordInfo* ri )
{
    // Check the data type
    if (ri->paramType != SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__STRING_TYPE)
    {
        printf("Capture directory must be a string\n");
        return;
    }

    // Set the parameter
    lockSinc(p);
    if (ri->v.stringValue[0] == 0)
    {
        ri->hasValue = true;
        strncpy(ri->v.stringValue, p->captureDir, EPICS_STRING_LEN);
    }
    else
    {
        strncpy(p->captureDir, ri->v.stringValue, EPICS_STRING_LEN);
    }

    p->captureDir[EPICS_STRING_LEN] = 0;
    unlockSinc(p);
}

// Get the capture file pattern.
void getCaptureFilePattern( falconXport* p, recordInfo* ri )
{
    lockSinc(p);
    ri->hasValue = true;
    strncpy( ri->v.stringValue, p->captureFilePattern, EPICS_STRING_LEN );
    ri->v.stringValue[EPICS_STRING_LEN] = 0;
    unlockSinc(p);
}

// Set the capture file pattern.
void setCaptureFilePattern( falconXport* p, recordInfo* ri )
{
    // Check the data type
    if (ri->paramType != SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__STRING_TYPE)
    {
        printf("Capture file pattern must be a string\n");
        return;
    }

    // Set the parameter
    lockSinc(p);
    if (ri->v.stringValue[0] == 0)
    {
        ri->hasValue = true;
        strncpy(ri->v.stringValue, p->captureFilePattern, EPICS_STRING_LEN);
    }
    else
    {
        strncpy(p->captureFilePattern, ri->v.stringValue, EPICS_STRING_LEN);
    }

    p->captureDir[EPICS_STRING_LEN] = 0;
    unlockSinc(p);
}

// Get a capture path section.
void getCapturePathSection( falconXport* p, recordInfo* ri, int section )
{
    lockSinc(p);
    ri->hasValue = true;
    strncpy( ri->v.stringValue, p->path[section], EPICS_STRING_LEN );
    ri->v.stringValue[EPICS_STRING_LEN] = 0;
    unlockSinc(p);
}

// Set a capture path section.
void setCapturePathSection( falconXport* p, recordInfo* ri, int section )
{
    // Check the data type
    if (ri->paramType != SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__STRING_TYPE)
    {
        printf("Capture path section must be a string\n");
        return;
    }

    // Set the parameter
    lockSinc(p);
    if (ri->v.stringValue[0] == 0)
    {
        ri->hasValue = true;
        strncpy(ri->v.stringValue, p->path[section], EPICS_STRING_LEN);
    }
    else
    {
        strncpy(p->path[section], ri->v.stringValue, EPICS_STRING_LEN);
    }

    p->path[section][EPICS_STRING_LEN] = 0;
    unlockSinc(p);
}

// Get the max capture file size.
void getMaxCaptureFileSize( falconXport* p, recordInfo* ri )
{
    lockSinc(p);
    ri->hasValue = true;
    ri->v.intValue = p->maxCaptureFileSize;
    unlockSinc(p);
}

// Set the max capture file size.
void setMaxCaptureFileSize( falconXport* p, recordInfo* ri )
{
    // Check the data type
    if (ri->paramType != SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__INT_TYPE)
    {
        printf("Max capture file size must be an int\n");
        return;
    }

    // Set the parameter
    lockSinc(p);
    if (ri->v.intValue == 0)
    {
        ri->hasValue = true;
        ri->v.intValue = p->maxCaptureFileSize;
    }
    else
    {
        p->maxCaptureFileSize = ri->v.intValue;
    }

    unlockSinc(p);
}

// Get the next run number.
void getNextRunNumber( falconXport* p, recordInfo* ri )
{
    ri->hasValue = true;
    ri->v.intValue = runNumberRead();
}

// Set the next run number.
void setNextRunNumber( falconXport* p, recordInfo* ri )
{
    // Check the data type
    if (ri->paramType != SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__INT_TYPE)
    {
        printf("Max capture file size must be an int\n");
        return;
    }

    // Set the parameter
    if (ri->v.intValue == 0)
    {
        ri->hasValue = true;
        ri->v.intValue = runNumberRead();
    }
    else
    {
        runNumberWrite(ri->v.intValue);
    }
}

// Get the max capture file size.
void getPulseCountRateCombined( falconXport* p, recordInfo* ri )
{
    int total = 0;
    int ch;

    for (ch = 0; ch < p->numChannels; ch++)
    {
        captureChannel *cc = &p->captureChannels[ch];
        pthread_mutex_lock(&cc->pulseCountRateMutex);
        total += cc->pulseCountRate;
        pthread_mutex_unlock(&cc->pulseCountRateMutex);
    }

    ri->hasValue = true;
    ri->v.intValue = total;
}

// Get the capture file pattern.
void getPulseCountRateIndividual( falconXport* p, recordInfo* ri )
{
    char *cpos = ri->v.stringValue;
    int ccount = 0;
    int ch;

    for (ch = 0; ch < p->numChannels; ch++)
    {
        captureChannel *cc = &p->captureChannels[ch];

        // Output the per channel count rate.
        pthread_mutex_lock(&cc->pulseCountRateMutex);
        int written = snprintf(cpos, EPICS_STRING_LEN - ccount, "%d", cc->pulseCountRate);
        if (written > 0)
        {
            ccount += written;
            cpos += written;
        }

        pthread_mutex_unlock(&cc->pulseCountRateMutex);

        // Add a comma all except the last time.
        if (ch < p->numChannels - 1 && EPICS_STRING_LEN > ccount)
        {
            *cpos++ = ',';
            ccount++;
        }
    }

    *cpos = 0;
    ri->hasValue = true;
}

// Start or stop oscilloscope
void oscilloscope( falconXport* p, recordInfo* ri )
{
    lockSinc(p);
    if (ri->v.binaryValue)
    {
        if (!SincStartOscilloscope( &p->sinc, channelId ) )
        {
           printf( "Error issuing Start Oscilloscope command for %s  Error: %d (%s)\n",
                   ri->record->name,
                   SincCurrentErrorCode( &p->sinc ),
                   SincCurrentErrorMessage( &p->sinc ) );
        }
    }
    else
    {
        if (!SincStopDataAcquisition( &p->sinc, channelId, 1000 ) )
        {
           printf( "Error issuing Stop Data Acquisition command for %s  Error: %d (%s)\n",
                   ri->record->name,
                   SincCurrentErrorCode( &p->sinc ),
                   SincCurrentErrorMessage( &p->sinc ) );
        }
    }

    unlockSinc(p);
}

// Start or stop histogram
void histogram( falconXport* p, recordInfo* ri )
{
  lockSinc(p);
  if( ri->v.binaryValue == true )
  {
    if( !SincStartHistogram( &p->sinc, channelId ) )
    {
       printf( "Error issuing Start Histogram command for %s  Error: %d (%s)\n",
               ri->record->name,
               SincCurrentErrorCode( &p->sinc ),
               SincCurrentErrorMessage( &p->sinc ) );
    }
  }
  else
  {
    if( !SincStopDataAcquisition( &p->sinc, channelId, 1000 ) )
    {
       printf( "Error issuing Stop Data Acquisition command for %s  Error: %d (%s)\n",
               ri->record->name,
               SincCurrentErrorCode( &p->sinc ),
               SincCurrentErrorMessage( &p->sinc ) );
    }
  }
  unlockSinc(p);
}

// Start or stop list mode
void listmode( falconXport* p, recordInfo* ri )
{
  lockSinc(p);
  if( ri->v.binaryValue == true )
  {
    if( !SincStartListMode( &p->sinc, channelId ) )
    {
       printf( "Error issuing Start List Mode command for %s  Error: %d (%s)\n",
               ri->record->name,
               SincCurrentErrorCode( &p->sinc ),
               SincCurrentErrorMessage( &p->sinc ) );
    }
  }
  else
  {
    if( !SincStopDataAcquisition( &p->sinc, channelId, 1000 ) )
    {
       printf( "Error issuing Stop Data Acquisition command for %s  Error: %d (%s)\n",
               ri->record->name,
               SincCurrentErrorCode( &p->sinc ),
               SincCurrentErrorMessage( &p->sinc ) );
    }
  }
  unlockSinc(p);
}

// Reset Spatial System
void rstsptlsstm( falconXport* p, recordInfo* ri )
{
  if( ri->v.binaryValue == true )
  {
    lockSinc(p);
    if( !SincResetSpatialSystem( &p->sinc ) )
    {
       printf( "Error issuing Reset Spatial System command for %s  Error: %d (%s)\n",
               ri->record->name,
               SincCurrentErrorCode( &p->sinc ),
               SincCurrentErrorMessage( &p->sinc ) );
    }

    unlockSinc(p);
  }
}

// Stop data acquisition (list mode, oscilloscope, histogram, calibration)
void stop( falconXport* p, recordInfo* ri )
{
    if( ri->v.binaryValue == true )
    {
        lockSinc(p);
        if( !SincStop( &p->sinc, channelId, 1000, true ) )
        {
            printf( "Error issuing Stop command for %s  Error: %d (%s)\n",
                    ri->record->name,
                    SincCurrentErrorCode( &p->sinc ),
                    SincCurrentErrorMessage( &p->sinc ) );
        }

        unlockSinc(p);
    }
}

// Restart the unit (reboot takes around one minute)
void restart( falconXport* p, recordInfo* ri )
{
    if( ri->v.binaryValue == true )
    {
        lockSinc(p);
        if( !SincRestart( &p->sinc ) )
        {
            printf( "Error issuing Restart command for %s  Error: %d (%s)\n",
                    ri->record->name,
                    SincCurrentErrorCode( &p->sinc ),
                    SincCurrentErrorMessage( &p->sinc ) );
        }

        unlockSinc(p);
    }
}

// Capture / stop capturing list mode data
void capture( falconXport* p, recordInfo* ri )
{
    switch (ri->v.intValue)
    {
   	case CS_COUNT_RATE:
   		// Start capturing but don't save to file.
        printf("starting count rate capture.\n");
        fxCaptureStart(p, CS_COUNT_RATE);
        break;
        
   	case CS_CAPTURE:
   		// Start capturing.
        printf("starting capture to files.\n");
        fxCaptureStart(p, CS_CAPTURE);
        break;
        
	case CS_OFF:
	default:
        // Stop capturing.
        printf("stopping capture.\n");
        fxCaptureStop(p);
    }
}

// Get capture status.
void getCaptureStatus( falconXport* p, recordInfo* ri )
{
    // Get the parameter
    Sinc* sinc = &p->sinc;
    SiToro__Sinc__GetParamResponse* resp = NULL;
    int fromChannelId;
    lockSinc(p);
    bool result = SincGetParam(sinc, channelId, "channel.state", &resp, &fromChannelId );

    // Log any errors
    if( result == 0 )
    {
        printf( "SincGetParam failed for %s for param: %d (%s)\n",
              "channel.state",
              SincCurrentErrorCode( sinc ),
              SincCurrentErrorMessage( sinc ) );
        unlockSinc(p);
        return;
    }

    unlockSinc(p);

    if( resp == NULL )
    {
        printf( "SincGetParam response for %s is NULL\n", "channel.state" );
        return;
    }

    if( resp->n_results == 0 )
    {
        printf( "SincGetParam response for %s contains no values\n", "channel.state" );
        si_toro__sinc__get_param_response__free_unpacked( resp, NULL );
        return;
    }

    // Get the first (and only) response
    SiToro__Sinc__KeyValue* kv = resp->results[0];

    // Extract the value and flag we have a value.
    if( !kv->optionval )
    {
        printf( "SincGetParam response for %s is not an option value\n", "channel.state" );
        si_toro__sinc__get_param_response__free_unpacked( resp, NULL );
        return;
    }

    ri->hasValue = true;
    ri->v.intValue = p->state;

    // Free the response.
    si_toro__sinc__get_param_response__free_unpacked( resp, NULL );
}

// Callback used for all record types
// Performs functionality that would block record processing
//
static void asyncActivityCallback( CALLBACK* callback )
{
    // Recover the record info
    recordInfo* ri;
    callbackGetUser( ri, callback );

    // Get the value if the port is OK
        //printf("asyncActivityCallback(%d, %s)\n", ri->function, ri->name);
    falconXport* p = getPort( ri );
    if( p )
    {
      // Perform the function required for this record
      switch( ri->function )
      {
        case RI_FUNCTION_GETPARAM:
          getParam( p, ri );
          break;

        case RI_FUNCTION_SETPARAM:
          setParam( p, ri );
          break;

        case RI_FUNCTION_OSCILLOSCOPE:
          oscilloscope( p, ri );
          break;

        case RI_FUNCTION_HISTOGRAM:
          histogram( p, ri );
          break;

        case RI_FUNCTION_LISTMODE:
          listmode( p, ri );
          break;

        case RI_FUNCTION_RSTSPTLSSTM:
          rstsptlsstm( p, ri );
          break;

        case RI_FUNCTION_STOP:
          stop( p, ri );
          break;

        case RI_FUNCTION_RESTART:
          restart( p, ri );
          break;

        case RI_FUNCTION_CAPTURE:
          capture( p, ri );
          break;

        case RI_FUNCTION_GET_CAPTURE_STATUS:
          getCaptureStatus( p, ri );
          break;

        case RI_FUNCTION_GET_CAPTURE_DIR:
          getCaptureDir( p, ri );
          break;

        case RI_FUNCTION_SET_CAPTURE_DIR:
          setCaptureDir( p, ri );
          break;

        case RI_FUNCTION_GET_CAPTURE_FILE_PATTERN:
          getCaptureFilePattern( p, ri );
          break;

        case RI_FUNCTION_SET_CAPTURE_FILE_PATTERN:
          setCaptureFilePattern( p, ri );
          break;

        case RI_FUNCTION_GET_CAPTURE_PATH_SECTION_1:
          getCapturePathSection( p, ri, 0 );
          break;

        case RI_FUNCTION_SET_CAPTURE_PATH_SECTION_1:
          setCapturePathSection( p, ri, 0 );
          break;

        case RI_FUNCTION_GET_CAPTURE_PATH_SECTION_2:
          getCapturePathSection( p, ri, 1 );
          break;

        case RI_FUNCTION_SET_CAPTURE_PATH_SECTION_2:
          setCapturePathSection( p, ri, 1 );
          break;

        case RI_FUNCTION_GET_CAPTURE_PATH_SECTION_3:
          getCapturePathSection( p, ri, 2 );
          break;

        case RI_FUNCTION_SET_CAPTURE_PATH_SECTION_3:
          setCapturePathSection( p, ri, 2 );
          break;

        case RI_FUNCTION_GET_CAPTURE_PATH_SECTION_4:
          getCapturePathSection( p, ri, 3 );
          break;

        case RI_FUNCTION_SET_CAPTURE_PATH_SECTION_4:
          setCapturePathSection( p, ri, 3 );
          break;

        case RI_FUNCTION_GET_MAX_CAPTURE_FILE_SIZE:
          getMaxCaptureFileSize( p, ri );
          break;

        case RI_FUNCTION_SET_MAX_CAPTURE_FILE_SIZE:
          setMaxCaptureFileSize( p, ri );
          break;

        case RI_FUNCTION_GET_NEXT_RUN_NUMBER:
          getNextRunNumber( p, ri );
          break;

        case RI_FUNCTION_SET_NEXT_RUN_NUMBER:
          setNextRunNumber( p, ri );
          break;

        case RI_GET_PULSE_COUNT_RATE_COMBINED:
          getPulseCountRateCombined( p, ri );
          break;

        case RI_GET_PULSE_COUNT_RATE_INDIVIDUAL:
          getPulseCountRateIndividual( p, ri );
          break;

        default:
          printf( "Unrecognised function (%d) in asyncActivityCallback for %s\n", ri->function, ri->record->name );
          break;
      }
    }

    // Record processing can now complete
    struct dbCommon* record = ri->record;
    struct rset* prset = (struct rset*)(record->rset);
    dbScanLock( record );
    (*prset->process)(record);
    dbScanUnlock( record );
}

// Find a FalconX port structure in falconXports given the IOC port name.
// Returns the index into falconXports if located, or -1 if not.
int findPort( const char* p )
{

  // Search through the defined ports
  // (warning, ports past portCount are undefined)
  int i;
  for( i = 0; i < portCount; i++ )
  {
    if( strcmp( p, falconXports[i].name ) == 0 )
    {
      // Found it. Return the index
      return( i );
    }
  }

  // Couldn't find it
  return -1;
}

// Parse the address specification.
//
// Generic form:   PORTNAME/FUNCTION/NAME
// Specific forms: PORTNAME/GETPARAM/PARAMNAME         $(PORT)/GETPARAM/instrument.temperature
// Specific forms: PORTNAME/SETPARAM/PARAMNAME         $(PORT)/SETPARAM/spatial.axis_1.enable
//
void parseAddress( const char* adr, recordInfo* ri )
{
  // Set default port info in case parsing fails
  ri->portIndex = -1;
  ri->name[0] = 0;

  // Proceed only if there is an address
  if( adr == NULL || adr[0] == '\0' )
  {
    printf( "No address specification for %s\n", ri->record->name );
    return;
  }

  // Copy the address so we can break it up
  char buf[MAX_ADDRESS_LEN+1];
  memset(buf, 0, sizeof(buf));
  strncpy(buf, adr, MAX_ADDRESS_LEN);

  // Break up the string
  char* t = strtok( buf, "/" );

  // Define states for parsing
  enum sections{ PORT, FUNCTION, NAME, NO_MORE };

  // Initial parsing state
  int i = PORT;

  // Parse address
  while( t != NULL )
  {
    switch( i )
    {
      // Find the named port
      case PORT:
        ri->portIndex = findPort( t );
        i = FUNCTION;
        break;

      // Match the function
      case FUNCTION:
        strncpy(ri->functionName, t, MAX_NAME_LEN);

        if( strcmp( t, "GETPARAM" ) == 0 )
        {
          ri->function = RI_FUNCTION_GETPARAM;
        }
        else if( strcmp( t, "SETPARAM" ) == 0 )
        {
          ri->function = RI_FUNCTION_SETPARAM;
        }
        else if( strcmp( t, "OSCILLOSCOPE" ) == 0 )
        {
          ri->function = RI_FUNCTION_OSCILLOSCOPE;
        }
        else if( strcmp( t, "HISTOGRAM" ) == 0 )
        {
          ri->function = RI_FUNCTION_HISTOGRAM;
        }
        else if( strcmp( t, "LISTMODE" ) == 0 )
        {
          ri->function = RI_FUNCTION_LISTMODE;
        }
        else if( strcmp( t, "RSTSPTLSSTM" ) == 0 )
        {
          ri->function = RI_FUNCTION_RSTSPTLSSTM;
        }
        else if( strcmp( t, "STOP" ) == 0 )
        {
          ri->function = RI_FUNCTION_STOP;
        }
        else if( strcmp( t, "RESTART" ) == 0 )
        {
          ri->function = RI_FUNCTION_RESTART;
        }
        else if( strcmp( t, "CAPTURE" ) == 0 )
        {
          ri->function = RI_FUNCTION_CAPTURE;
        }
        else if( strcmp( t, "GET_CAPTURE_STATUS" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_STATUS;
        }
        else if( strcmp( t, "GET_CAPTURE_DIR" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_DIR;
        }
        else if( strcmp( t, "SET_CAPTURE_DIR" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_CAPTURE_DIR;
        }
        else if( strcmp( t, "GET_CAPTURE_FILE_PATTERN" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_FILE_PATTERN;
        }
        else if( strcmp( t, "SET_CAPTURE_FILE_PATTERN" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_CAPTURE_FILE_PATTERN;
        }
        else if( strcmp( t, "GET_CAPTURE_PATH_SECTION_1" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_PATH_SECTION_1;
        }
        else if( strcmp( t, "SET_CAPTURE_PATH_SECTION_1" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_CAPTURE_PATH_SECTION_1;
        }
        else if( strcmp( t, "GET_CAPTURE_PATH_SECTION_2" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_PATH_SECTION_2;
        }
        else if( strcmp( t, "SET_CAPTURE_PATH_SECTION_2" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_CAPTURE_PATH_SECTION_2;
        }
        else if( strcmp( t, "GET_CAPTURE_PATH_SECTION_3" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_PATH_SECTION_3;
        }
        else if( strcmp( t, "SET_CAPTURE_PATH_SECTION_3" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_CAPTURE_PATH_SECTION_3;
        }
        else if( strcmp( t, "GET_CAPTURE_PATH_SECTION_4" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_CAPTURE_PATH_SECTION_4;
        }
        else if( strcmp( t, "SET_CAPTURE_PATH_SECTION_4" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_CAPTURE_PATH_SECTION_4;
        }
        else if( strcmp( t, "GET_MAX_CAPTURE_FILE_SIZE" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_MAX_CAPTURE_FILE_SIZE;
        }
        else if( strcmp( t, "SET_MAX_CAPTURE_FILE_SIZE" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_MAX_CAPTURE_FILE_SIZE;
        }
        else if( strcmp( t, "GET_NEXT_RUN_NUMBER" ) == 0 )
        {
          ri->function = RI_FUNCTION_GET_NEXT_RUN_NUMBER;
        }
        else if( strcmp( t, "SET_NEXT_RUN_NUMBER" ) == 0 )
        {
          ri->function = RI_FUNCTION_SET_NEXT_RUN_NUMBER;
        }
        else if( strcmp( t, "GET_PULSE_COUNT_RATE_COMBINED" ) == 0 )
        {
          ri->function = RI_GET_PULSE_COUNT_RATE_COMBINED;
        }
        else if( strcmp( t, "GET_PULSE_COUNT_RATE_INDIVIDUAL" ) == 0 )
        {
          ri->function = RI_GET_PULSE_COUNT_RATE_INDIVIDUAL;
        }
#ifdef NEVER
        else if( strcmp( t, "???" ) == 0 )
        {
          ri->function = RI_FUNCTION_???;
        }
#endif
        else
        {
          printf( "Unrecognised function '%s' in address specification '%s' for %s\n", t, adr, ri->record->name );
        }
        i = NAME;
        break;

      // Save the name. This will be a parameter name for get and set parameter functions. Perhaps something else for other functions
      case NAME:
        strncpy(ri->name, t, MAX_NAME_LEN);
        i = NO_MORE;
        break;

      // Shouldn't get here.
      case NO_MORE:
      default:
        printf( "Too many sections in INP or OUT field for %s. Should be PORT/FUNCTION/NAME. is: %s\n", ri->record->name, adr );
        break;
    }

// step on to the next token
    t = strtok( NULL, "/" );
  }
}

// Create and initialise the structure holding driver related information for a PV
recordInfo* init_recordInfo( const char* adr,                               // Address specification (input or output)
                             SiToro__Sinc__KeyValue__ParamType paramTypeIn, // parameter type ( used when reading and writing parameters. Set to  SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__NO_TYPE if not reading or writing parameters
                             dbCommon* recordIn )                           // Epics record
{
    // Allocate
    recordInfo* ri = malloc( sizeof( recordInfo ) );

    // Initialise
    ri->hasValue = false;
    ri->paramType = paramTypeIn;
    ri->record = recordIn;
    ri->function = RI_FUNCTION_UNDEFINED;
    parseAddress( adr, ri );

    // Return the new record information structure
    return ri;
}

//==========================================================================
// Record initialisation functions

static long init_airecord( struct aiRecord* ai )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( ai->inp.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( ai->inp.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__FLOAT_TYPE, (dbCommon*)ai );
        callbackSetUser( ri, callback );
        ai->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)ai, "devAifalconX (init_airecord) Illegal INP field");
        return( S_db_badField );
    }
    return( 0 );
}

static long init_birecord( struct biRecord* bi )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( bi->inp.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( bi->inp.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__BOOL_TYPE, (dbCommon*)bi );
        callbackSetUser( ri, callback );
        bi->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)bi, "devBifalconX (init_birecord) Illegal INP field" );
        return( S_db_badField );
    }
    return( 0 );
}

static long init_mbbirecord( struct mbbiRecord* mbbi )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( mbbi->inp.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( mbbi->inp.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__OPTION_TYPE, (dbCommon*)mbbi );
        callbackSetUser( ri, callback );
        mbbi->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField,(void*)mbbi, "devMbbifalconX (init_mbbirecord) Illegal INP field" );
        return( S_db_badField );
    }
    return( 0 );
}

static long init_stringinrecord( struct stringinRecord* stringin )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( stringin->inp.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( stringin->inp.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__STRING_TYPE, (dbCommon*)stringin );
        callbackSetUser( ri, callback );
        stringin->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)stringin, "devStringinfalconX (init_stringinrecord) Illegal INP field" );
        return( S_db_badField );
    }
    return( 0 );
}

static long init_longinrecord( struct longinRecord* longin )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( longin->inp.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( longin->inp.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__INT_TYPE, (dbCommon*)longin );
        callbackSetUser( ri, callback );
        longin->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)longin, "devLonginfalconX (init_longinrecord) Illegal INP field");
        return( S_db_badField );
    }
    return( 0 );
}

static long init_aorecord( struct aoRecord* ao )
{
    CALLBACK*callback;
    recordInfo* ri;

    switch ( ao->out.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( ao->out.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__FLOAT_TYPE, (dbCommon*)ao );
        callbackSetUser( ri, callback );
        ao->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)ao, "devAofalconX (init_aorecord) Illegal OUT field");
        return( S_db_badField );
    }
    return( 0 );
}

static long init_borecord( struct boRecord* bo )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( bo->out.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( bo->out.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__BOOL_TYPE, (dbCommon*)bo );
        callbackSetUser( ri, callback );
        bo->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)bo, "devBofalconX (init_borecord) Illegal OUT field" );
        return( S_db_badField );
    }
    return( 0 );
}

static long init_mbborecord( struct mbboRecord* mbbo )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( mbbo->out.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( mbbo->out.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__OPTION_TYPE, (dbCommon*)mbbo );
        callbackSetUser( ri, callback );
        mbbo->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)mbbo, "devMbbofalconX (init_mbborecord) Illegal OUT field" );
        return( S_db_badField );
    }
    return( 0 );
}

static long init_stringoutrecord( struct stringoutRecord* stringout )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( stringout->out.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( stringout->out.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__STRING_TYPE, (dbCommon*)stringout );
        callbackSetUser( ri, callback );
        stringout->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)stringout, "devStringoutfalconX (init_stringoutrecord) Illegal OUT field" );
        return( S_db_badField );
    }
    return( 0 );
}

static long init_longoutrecord( struct longoutRecord* longout )
{
    CALLBACK* callback;
    recordInfo* ri;

    switch ( longout->out.type )
    {
      case (INST_IO):
        callback = (CALLBACK*)(calloc( 1, sizeof(CALLBACK) ));
        callbackSetCallback( asyncActivityCallback, callback );
        ri = init_recordInfo( longout->out.text, SI_TORO__SINC__KEY_VALUE__PARAM_TYPE__INT_TYPE, (dbCommon*)longout );
        callbackSetUser( ri, callback );
        longout->dpvt = (void*)callback;
        break;

      default:
        recGblRecordError( S_db_badField, (void*)longout, "devLongoutfalconX (init_longoutrecord) Illegal OUT field" );
        return( S_db_badField );
    }
    return( 0 );
}

//==========================================================================
// Record read functions

static long read_ai( struct aiRecord* ai )
{
    CALLBACK* callback = (CALLBACK*)ai->dpvt;

    // If processing, value is now available (hopefully)
    // If value is available, put it in the record
    if( ai->pact )
    {
        recordInfo* ri;
        callbackGetUser( ri, callback );

        if( ri->hasValue )
        {
              ai->val = ri->v.floatValue;
              ai->udf = FALSE;
        }
        return( 2 ); /* don't convert*/
    }

    // Request read
    ai->pact=TRUE;
    callbackRequest( callback );
    return( 0 );
}


static long read_bi(struct biRecord* bi )
{
    CALLBACK* callback = (CALLBACK*)bi->dpvt;

    // If processing, value is now available (hopefully)
    // If value is available, put it in the record
    if( bi->pact )
    {
        recordInfo* ri;
        callbackGetUser( ri, callback );

        if( ri->hasValue )
        {
              bi->val = ri->v.binaryValue;
              bi->udf = FALSE;
        }
        return( 2 ); /* don't convert*/
    }

    // Request read
    bi->pact=TRUE;
    callbackRequest( callback );
    return( 0 );
}

static long read_mbbi( struct mbbiRecord* mbbi )
{
    CALLBACK* callback = (CALLBACK*)mbbi->dpvt;

    // If processing, value is now available (hopefully)
    // If value is available, put it in the record
    if( mbbi->pact )
    {
        recordInfo* ri;
        callbackGetUser( ri, callback );

        if( ri->hasValue )
        {
              int i;
              for( i = 0; i < 16; i++ )
              {
                char* str = (&mbbi->zrst)[i];  // Note, zrst and following 15 states must be contiguous
                if( !strcmp( str, ri->v.stringValue ) )
                {
                  mbbi->val = i;
                  mbbi->udf = FALSE;
                  break;
                }
              }
        }
        return( 2 ); /* don't convert*/
    }

    // Request read
    mbbi->pact=TRUE;
    callbackRequest( callback );
    return( 0 );
}

static long read_stringin( struct stringinRecord* stringin )
{
    CALLBACK* callback = (CALLBACK*)stringin->dpvt;

    // If processing, value is now available (hopefully)
    // If value is available, put it in the record
    if( stringin->pact )
    {
        recordInfo* ri;
        callbackGetUser( ri, callback );

        if( ri->hasValue )
        {
            strcpy( stringin->val, ri->v.stringValue );
            stringin->udf = FALSE;
        }
        return( 2 ); /* don't convert*/
    }

    // Request read
    stringin->pact=TRUE;
    callbackRequest( callback );
    return( 0 );
}

static long read_longin( struct longinRecord* longin )
{
    CALLBACK* callback = (CALLBACK*)longin->dpvt;

    // If processing, value is now available (hopefully)
    // If value is available, put it in the record
    if( longin->pact )
    {
        recordInfo* ri;
        callbackGetUser( ri, callback );

        if( ri->hasValue )
        {
              longin->val = ri->v.intValue;
              longin->udf = FALSE;
        }
        return( 2 ); /* don't convert*/
    }

    // Request read
    longin->pact=TRUE;
    callbackRequest( callback );
    return( 0 );
}


//==========================================================================
// Record write functions

static long write_ao( struct aoRecord* ao )
{
    CALLBACK* callback = (CALLBACK*)ao->dpvt;

    // If processing, write is now complete
    if( ao->pact )
    {
        ao->udf = FALSE;
        return( 2 ); /* don't convert*/
    }

    // Start processing
    ao->pact=TRUE;

    // Set up value for writing
    recordInfo* ri;
    callbackGetUser( ri, callback );
    ri->v.floatValue = ao->val;

    // Request write
    callbackRequest( callback );
    return( 0 );
}

static long write_bo( struct boRecord* bo )
{
    CALLBACK* callback = (CALLBACK*)bo->dpvt;

    // If processing, write is now complete
    if( bo->pact )
    {
        bo->udf = FALSE;
        return( 2 ); /* don't convert*/
    }

    // Start processing
    bo->pact=TRUE;

    // Set up value for writing
    recordInfo* ri;
    callbackGetUser( ri, callback );
    ri->v.binaryValue = bo->val;

    // Request write
    callbackRequest( callback );
    return( 0 );
}

static long write_mbbo( struct mbboRecord* mbbo )
{
    CALLBACK* callback = (CALLBACK*)mbbo->dpvt;

    // If processing, write is now complete
    if( mbbo->pact )
    {
        mbbo->udf = FALSE;
        return( 2 ); /* don't convert*/
    }

    // Start processing
    mbbo->pact=TRUE;

    // Set up value for writing
    recordInfo* ri;
    callbackGetUser( ri, callback );

//    int step = (int)((unsigned long)(&mbbo->onst) - (unsigned long)(&mbbo->zrst));

    int i =  mbbo->val;
    if( i >= 0 && i < 16 )
    {
      // Value found, set it up
      strncpy( ri->v.stringValue, (&mbbo->zrst)[i], EPICS_STRING_LEN ); // Note, zrst and following 15 states must be contiguous
      ri->v.stringValue[EPICS_STRING_LEN] = '\0';
    }
    else
    {
      // Value not fouond, abort writing
      mbbo->pact=FALSE;
      return( 0 );
    }

    // Request write
    callbackRequest( callback );
    return( 0 );
}

static long write_stringout( struct stringoutRecord* stringout )
{
    CALLBACK* callback = (CALLBACK*)stringout->dpvt;

    // If processing, write is now complete
    if( stringout->pact )
    {
        stringout->udf = FALSE;

        return( 2 ); /* don't convert*/
    }

    // Start processing
    stringout->pact=TRUE;

    // Set up value for writing
    recordInfo* ri;
    callbackGetUser( ri, callback );
    strncpy( ri->v.stringValue, stringout->val, EPICS_STRING_LEN );
    ri->v.stringValue[EPICS_STRING_LEN] = '\0';

    // Request write
    callbackRequest( callback );
    return( 0 );
}

static long write_longout( struct longoutRecord* longout )
{
    CALLBACK* callback = (CALLBACK*)longout->dpvt;

    // If processing, write is now complete
    if( longout->pact )
    {
        longout->udf = FALSE;
        return( 2 ); /* don't convert*/
    }

    // Start processing
    longout->pact=TRUE;

    // Set up value for writing
    recordInfo* ri;
    callbackGetUser( ri, callback );
    ri->v.intValue = longout->val;

    // Request write
    callbackRequest( callback );
    return( 0 );
}

//==========================================================================

// EPICS iocsh callable function to configure a portCount
// Typical line would be:
//    falconXPortConfigure("X1","10.114.2.50")
int falconXPortConfigure( const char* portName,   // For example, X1
                          const char* IPAddress ) // For example, 10.114.2.50
{
    // Check port name is available
    if( portName == NULL || portName[0] == '\0' )
    {
        printf( "falconXPortConfigure() - portName is NULL or empty\n" );
        return 0;
    }

    // Check address is available
    if( IPAddress == NULL || IPAddress[0] == '\0' )
    {
        printf( "falconXPortConfigure() - IPAddress is NULL or empty for port name %s\n", portName );
        return 0;
    }

    // Add port information
    falconXport* p = &falconXports[portCount];
    portCount++;

    memset(p, 0, sizeof(*p));

    strncpy(p->name, portName, MAX_NAME_LEN);
    strncpy(p->address, IPAddress, MAX_IP_LEN);
    p->captureThreadRunning = false;
    p->state = CS_OFF;
    p->packetFilter = 0;
    p->splitCount = 0;

    // Read the port config.
    configRead(p);

    pthread_mutex_init(&p->mutex, NULL);
    pthread_mutex_init(&p->captureMutex, NULL);

    // Attempt connection
    SincInit(&p->sinc);
    SincSetTimeout(&p->sinc, SINC_TIMEOUT);

    lockSinc(p);
    if( !SincConnect(&p->sinc, p->address, SINC_PORT ) )
    {
        printf( "falconXPortConfigure() - Can't connect to %s:%d - %s\n", p->address, SINC_PORT, SincCurrentErrorMessage( &p->sinc ) );
        return 0;
    }

    // Get the number of channels supported by the device.
    SiToro__Sinc__GetParamResponse *resp = NULL;
    if (!SincGetParam(&p->sinc, 0, "instrument.numChannels", &resp, NULL) || resp->n_results < 1 || !resp->results[0]->has_intval)
    {
        printf( "falconXPortConfigure() - Can't get numChannels - %s\n", SincCurrentErrorMessage(&p->sinc) );
        unlockSinc(p);
        return 0;
    }

    p->numChannels = resp->results[0]->intval;

    // Load the FalconX device config file if one exists.
    loadDeviceConfig(p);

    unlockSinc(p);

    return( 0 );
}


/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",   iocshArgString};
static const iocshArg initArg1 = { "IP address", iocshArgString};
static const iocshArg* const initArgs[] = {&initArg0,
                                           &initArg1};
static const iocshFuncDef initFuncDef = {"falconXPortConfigure", 2, initArgs};
static void initCallFunc( const iocshArgBuf* args )
{
    falconXPortConfigure(args[0].sval, args[1].sval);
}

void falconXDriverRegister( void )
{
    iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(falconXDriverRegister);

