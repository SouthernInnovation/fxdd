#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "falconx.h"
#include "sinc.h"
#include "lmbuf.h"


//
// Requests the capture thread to stop.
//

static void requestStopCapturing(falconXport *p)
{
    pthread_mutex_lock(&p->captureMutex);
    p->state = CS_OFF;
    pthread_mutex_unlock(&p->captureMutex);
}


//
// Checks if the capture thread is still running. Poll this after
// calling stopRunning() so you know when it's stopped.
//

static bool isRunning(falconXport *p)
{
    pthread_mutex_lock(&p->captureMutex);
    bool result = p->captureThreadRunning;
    pthread_mutex_unlock(&p->captureMutex);
    return result;
}


//
// Check the current capture state.
//

static void setState(falconXport *p, enum captureState v)
{
    pthread_mutex_lock(&p->captureMutex);
    p->state = v;
    pthread_mutex_unlock(&p->captureMutex);
}


//
// Check the current capture state.
//

static enum captureState getState(falconXport *p)
{
    pthread_mutex_lock(&p->captureMutex);
    enum captureState result = p->state;
    pthread_mutex_unlock(&p->captureMutex);
    return result;
}


//
// Substitutes specific substrings in a path with run information.
//
//    $HOME is replaced by the home directory.
//    $RUN is replaced by the run number.
//    $SEGMENT is replaced by the segment number.
//    $WHEN is replaced by the run start date/time.
//    $DETECTOR is replaced by the detector id.
//
// dest must be of size PATH_MAX + 1.
//

static void substituteVars(char *dest, const char *src, falconXport *p, int channelId)
{
    struct tm *startTm;
    char *dp = dest;

    while (dp - dest < PATH_MAX && *src != 0)
    {
        if (*src != '$')
        {
            // Just a normal character. Copy it.
            *dp++ = *src++;
        }
        else
        {
            // Could be a replacement string.
            size_t ds = dest + PATH_MAX - dp;

            if (strncmp(src, "$HOME", 5) == 0)
            {
                strncpy(dp, getenv("HOME"), ds);
                dp += strlen(dp);
                src += 5;
            }
            else if (strncmp(src, "$RUN", 4) == 0)
            {
                dp += snprintf(dp, ds, "%05d", p->runNumber);
                src += 4;
            }
            else if (strncmp(src, "$SEGMENT", 8) == 0)
            {
                dp += snprintf(dp, ds, "%06d", p->splitCount + 1);
                src += 8;
            }
            else if (strncmp(src, "$WHEN", 5) == 0)
            {
                startTm = localtime(&p->captureStartTime);

                dp += strftime(dp, ds, "%F_%T", startTm);
                src += 5;
            }
            else if (strncmp(src, "$DETECTOR", 9) == 0)
            {
                dp += snprintf(dp, ds, "%02d", channelId);
                src += 9;
            }
            else if (strncmp(src, "$PATH1", 6) == 0)
            {
                strncpy(dp, p->path[0], ds);
                dp += strlen(dp);
                src += 6;
            }
            else if (strncmp(src, "$PATH2", 6) == 0)
            {
                strncpy(dp, p->path[1], ds);
                dp += strlen(dp);
                src += 6;
            }
            else if (strncmp(src, "$PATH3", 6) == 0)
            {
                strncpy(dp, p->path[2], ds);
                dp += strlen(dp);
                src += 6;
            }
            else if (strncmp(src, "$PATH4", 6) == 0)
            {
                strncpy(dp, p->path[3], ds);
                dp += strlen(dp);
                src += 6;
            }
            else
            {
                // Not a string we recognise.
                *dp++ = *src++;
            }
        }
    }

    *dp = 0;
}


//
// Create a path name from the capture directory and file pattern.
//

static void makeFilePath(falconXport *p, int channelId, char *pathName)
{
    char captureDir[PATH_MAX+1];
    char captureFilePattern[PATH_MAX+1];

    substituteVars(captureDir, p->captureDir, p, channelId);
    substituteVars(captureFilePattern, p->captureFilePattern, p, channelId);

    snprintf(pathName, PATH_MAX, "%s/%s", captureDir, captureFilePattern);
}


//
// Check that the directory for the named path exists and if it
// doesn't, make all the required parent directories.
//

bool ensureDirectoryExists(char *path)
{
    char       *pp;
    char       *sp;
    bool        failed = false;
    char        copypath[PATH_MAX];
    struct stat st;

    memset(copypath, 0, sizeof(copypath));
    strncpy(copypath, path, PATH_MAX-1);

    pp = copypath;
    while (!failed && (sp = strchr(pp, '/')) != 0)
    {
        if (sp != pp)
        {
            /* Neither root nor double slash in path */
            *sp = '\0';

            if (stat(copypath, &st) != 0)
            {
                /* Directory does not exist. EEXIST for race condition */
                if (mkdir(copypath, 0775) != 0 && errno != EEXIST)
                {
                    failed = true;
                }
            }
            else if (!S_ISDIR(st.st_mode))
            {
                errno = ENOTDIR;
                failed = true;
            }

            *sp = '/';
        }

        pp = sp + 1;
    }

    return !failed;
}


//
// Open a file with all the time and part information in the name.
//

static FILE *openListModeFile(falconXport *p, int channelId)
{
    char pathName[PATH_MAX+1];

    // Create the path.
    memset(pathName, 0, sizeof(pathName));
    makeFilePath(p, channelId, pathName);

    // Make sure the directory exists.
    ensureDirectoryExists(pathName);

    // Open the file.
    FILE *outFile = fopen(pathName, "w");
    if (outFile == NULL)
    {
        fprintf(stderr, "can't open %s\n", pathName);
        return NULL;
    }

    return outFile;
}


//
// Save a header to a list mode save file.
//

static size_t saveListModeHeader(falconXport *p, int channelId)
{
    uint8_t *headerData = NULL;
    size_t   headerLen = 0;

    /* Get the header. */
    if (!SincListModeEncodeHeader(&p->captureSinc, channelId, &headerData, &headerLen))
    {
        printf("can't create list mode header for channel %d\n", channelId);
        return 0;
    }

    /* Save it to the file. */
    FILE *outFile = p->captureChannels[channelId].outFile;
    fwrite(headerData, 1, headerLen, outFile);
    fflush(outFile);

    /* Clean up. */
    free(headerData);

    return headerLen;
}


//
// Close any list mode files which are open.
//

static void closeListModeFiles(falconXport *p)
{
    // Close any files which are open.
    int ch;
    for (ch = 0; ch < p->numChannels; ch++)
    {
        captureChannel *cc = &p->captureChannels[ch];

        if (cc->outFile)
        {
            fclose(cc->outFile);
            cc->outFile = NULL;
        }

        // Clear the byte counts.
        p->captureChannels[ch].writtenBytes = 0;
    }
}


//
// Open all of the list mode files and create headers for them.
//

static void openListModeFiles(falconXport *p, bool saveHeader)
{
    // Close any files which are open.
    closeListModeFiles(p);

    // Open the new files
    int ch;
    for (ch = 0; ch < p->numChannels; ch++)
    {
        p->captureChannels[ch].writtenBytes = 0;
        p->captureChannels[ch].outFile = openListModeFile(p, ch);

        if (saveHeader)
        {
            p->captureChannels[ch].writtenBytes = saveListModeHeader(p, ch);
        }
    }
}


//
// Initialises list mode data capture and data monitoring.
//

static bool startCapturing(falconXport *p, bool saveToFile)
{
    // Make a new SINC connection just for data.
    if (!SincConnect(&p->captureSinc, p->address, SINC_PORT))
    {
        printf("Couldn't connect to %s - %s\n", p->address, SincCurrentErrorMessage(&p->captureSinc));
        return false;
    }

    // Stop all channels.
    int ch;
    for (ch = 0; ch < p->numChannels; ch++)
    {
        SincStop(&p->captureSinc, ch, SINC_TIMEOUT, false);
    }

    // Start all the channels.
    for (ch = 0; ch < p->numChannels; ch++)
    {
        if (!SincStartListMode(&p->captureSinc, ch))
        {
            printf("Couldn't start list mode on %s channel %d - %s\n", p->address, ch, SincCurrentErrorMessage(&p->captureSinc));
            return false;
        }
    }

    // Note what time we started capturing.
    p->captureStartTime = time(NULL);

    // Open the result files.
    if (saveToFile)
    {
        openListModeFiles(p, true);
    }

    // Now start monitoring the data in a single hit.
    int chanSet[MAX_CHANNELS];
    for (ch = 0; ch < p->numChannels; ch++)
    {
        chanSet[ch] = ch;
    }

    if (!SincMonitorChannels(&p->captureSinc, chanSet, p->numChannels))
    {
        printf("Couldn't monitor channels on %s - %s\n", p->address, SincCurrentErrorMessage(&p->captureSinc));
        return false;
    }

    return true;
}


//
// Stops list mode data capture and data monitoring.
// Called from the capture thread.
//

static void stopCapturing(falconXport *p)
{
    if (SincIsConnected(&p->captureSinc))
    {
        // Stop receiving data while we stop the channels.
        if (!SincMonitorChannels(&p->captureSinc, NULL, 0))
        {
            printf("Couldn't stop monitoring channels on %s - %s\n", p->address, SincCurrentErrorMessage(&p->captureSinc));
        }

        // Stop all channels.
        int ch;
        for (ch = 0; ch < p->numChannels; ch++)
        {
            SincStop(&p->captureSinc, ch, p->captureSinc.timeout, false);
        }

        // Disconnect.
        SincDisconnect(&p->captureSinc);
    }

    // Close all the files.
    closeListModeFiles(p);

    // Mark us as "not running".
    pthread_mutex_lock(&p->captureMutex);
    p->captureThreadRunning = false;
    p->state = CS_OFF;
    pthread_mutex_unlock(&p->captureMutex);
}


//
// If we've just received a "parameter updated" response, see if it's
// telling us that list mode has stopped for some reason - probably
// someone stopped it externally.
//

static bool checkIfListModeStopped(Sinc *sinc)
{
    /* Optionally check if list mode has stopped for some reason. */
    unsigned int j;
    bool done = false;

    SiToro__Sinc__ParamUpdatedResponse *resp = NULL;
    if (!SincReadParamUpdatedResponse(sinc, sinc->timeout, &resp, NULL))
    {
        printf("can't read parameter update - %s\n", SincCurrentErrorMessage(sinc));
        return true;
    }

    for (j = 0; j < resp->n_params; j++)
    {
        SiToro__Sinc__KeyValue *param = resp->params[j];
        if (strcmp(param->key, "channel.state") == 0 && param->optionval != NULL && strcmp(param->optionval, "ready") == 0)
        {
            done = true;
        }
    }

    si_toro__sinc__param_updated_response__free_unpacked(resp, NULL);

    return done;
}


//
// Write a single list mode data packet to a file.
//

static void writeListModePacketToFile(int chId, uint8_t *lmData, size_t lmDataLen, falconXport *p)
{
    captureChannel *cc = &p->captureChannels[chId];

    // Open a new file if necessary.
    if (cc->outFile == NULL)
    {
        openListModeFiles(p, false);
    }

    // Do we need to split the output file and increment the file "part"?
    if (p->maxCaptureFileSize > 0 && (cc->writtenBytes + lmDataLen) >= p->maxCaptureFileSize * 1000000)
    {
        // Start files with a new part number.
        p->splitCount++;
        openListModeFiles(p, false);
    }

    // Write this list mode packet to a file.
    fwrite(lmData, 1, lmDataLen, cc->outFile);
    cc->writtenBytes += lmDataLen;
}


//
// We've received a list mode data message.
//

static void readListModeMessage(falconXport *p, enum captureState state)
{
    // Read the list mode message.
    int chId = 0;
    uint8_t *data = NULL;
    int dataLen = 0;
    uint64_t dataSetId = 0;

    if (!SincReadListMode(&p->captureSinc, p->captureSinc.timeout, &chId, &data, &dataLen, &dataSetId) &&
            SincCurrentErrorCode(&p->captureSinc) != SI_TORO__SINC__ERROR_CODE__TIMEOUT)
    {
        printf("can't get list mode data - %s\n", SincCurrentErrorMessage(&p->captureSinc));
        return;
    }

    // Was there some data?
    if (data == NULL)
        return;

    // Add the data to the list mode decoder buffer.
    captureChannel *cc = &p->captureChannels[chId];
    LmBuf *lmBuf = &cc->decodeBuf;
    LmBufAddData(lmBuf, data, dataLen);

    // Interpret list mode packets off the buffer until we can take no more.
    LmPacket listModePacket;

    while (LmBufGetNextPacket(lmBuf, &listModePacket))
    {
        // Is this packet one of the ones we're filtering out?
        if (state == CS_CAPTURE && ((1 << listModePacket.typ) & p->packetFilter) == 0)
        {
            // It's a packet we want. Write the raw data from this packet.
            uint8_t lmData[WRITE_BUF_SIZE];
            size_t lmDataLen = LmBufEncodePacket(lmData, WRITE_BUF_SIZE, &listModePacket, LM_VERSION_0_8_1); 
            if (lmDataLen > 0)
            {
                writeListModePacketToFile(chId, lmData, lmDataLen, p);
            }
        }

        // Is this a pulse?
        if (listModePacket.typ == LmPacketTypePulse)
        {
            // Add it to the pulse count stats.
            cc->accumCount++;
        }
    }

    // Free the data.
    free(data);

    // Update the stats.
    time_t now = time(NULL);
    if (now != cc->accumLastUpdate)
    {
        pthread_mutex_lock(&cc->pulseCountRateMutex);
        cc->pulseCountRate = cc->accumCount;
        pthread_mutex_unlock(&cc->pulseCountRateMutex);

        cc->accumCount = 0;
        cc->accumLastUpdate = now;
    }
}


//
// The main capture thread. Sets up capture, captures data and
// then closes capture.
//

static void *fxCaptureThread(void *portVoid)
{
    falconXport *p = (falconXport *)portVoid;
    Sinc *sinc = &p->captureSinc;
    enum captureState currentState = getState(p);

    // Start list mode capture on all channels.
    if (!startCapturing(p, currentState == CS_CAPTURE))
    {
        stopCapturing(p);
        return NULL;
    }

    // Keep capturing until we're told to quit.
    bool done = false;
    while (currentState != CS_OFF && !done)
    {
        // What type of packet is next?
        SiToro__Sinc__MessageType packetType = SI_TORO__SINC__MESSAGE_TYPE__NO_MESSAGE_TYPE;
        if (!SincPacketPeek(sinc, PACKET_POLL_PERIOD, &packetType))
        {
            if (SincCurrentErrorCode(sinc) == SI_TORO__SINC__ERROR_CODE__TIMEOUT)
            {
                // Timeout - loop back to re-check if stop was requested.
                packetType = SI_TORO__SINC__MESSAGE_TYPE__NO_MESSAGE_TYPE;
            }
            else
            {
                // Errored - fail out and stop capturing.
                printf("can't read capture packet - %s\n", SincCurrentErrorMessage(&p->captureSinc));
                done = true;
                packetType = SI_TORO__SINC__MESSAGE_TYPE__NO_MESSAGE_TYPE;
            }
        }

        // Have we changed to capturing to files?
        enum captureState newState = getState(p);
        if (newState == CS_CAPTURE && currentState != CS_CAPTURE)
        {
            // Start saving to files.
            openListModeFiles(p, true);
            printf("changed to capturing to files.\n");
        }

        switch (packetType)
        {
        case SI_TORO__SINC__MESSAGE_TYPE__NO_MESSAGE_TYPE:
            // Timed out or errored - just loop back and re-check for stop requested.
            break;

        case SI_TORO__SINC__MESSAGE_TYPE__PARAM_UPDATED_RESPONSE:
            // A parameter was updated - maybe it's telling us list mode has been stopped?
            if (checkIfListModeStopped(sinc))
            {
                printf("list mode was stopped externally - stopping capture");
                done = true;
            }
            break;

        case SI_TORO__SINC__MESSAGE_TYPE__LIST_MODE_DATA_RESPONSE:
            // Got a list mode message.
            readListModeMessage(p, newState);
            break;

        case SI_TORO__SINC__MESSAGE_TYPE__ASYNCHRONOUS_ERROR_RESPONSE:
            // An unexpected error.
            if (!SincReadAsynchronousErrorResponse(sinc, sinc->timeout, NULL, NULL))
            {
                printf("device error - %s\n", SincCurrentErrorMessage(&p->captureSinc));
                done = true;
            }
            break;

        default:
            // It's a message type we don't care about - toss it.
            if (!SincReadAndDiscardPacket(sinc, sinc->timeout))
            {
                printf("can't read from device - %s\n", SincCurrentErrorMessage(&p->captureSinc));
                done = true;
            }
            break;
        }

        // Do we need to stop capturing to files?
        if (newState == CS_COUNT_RATE && currentState != CS_COUNT_RATE)
        {
            // Close the files.
            closeListModeFiles(p);
            printf("now capturing for count rate only.\n");
        }

        currentState = newState;
    }

    // Stop everything.
    stopCapturing(p);

    return NULL;
}


//
// External interface to start capture. Starts the capture thread.
// Called from a client thread.
//

void fxCaptureStart(falconXport *p, enum captureState newCaptureState)
{
    int ch;
    bool saveToFile = newCaptureState == CS_CAPTURE;

    // Is it already running?
    if (!isRunning(p))
    {
        // Initialise some fields.
        pthread_mutex_lock(&p->captureMutex);
        p->captureThread = 0;
        p->captureThreadRunning = false;
        p->splitCount = 0;

        if (saveToFile)
        {
            p->runNumber = runNumberReadAndIncrement(p);
        }
        else
        {
            p->runNumber = runNumberRead(p);
        }

        SincInit(&p->captureSinc);

        for (ch = 0; ch < p->numChannels; ch++)
        {
            // Initialise a channel.
            captureChannel *cc = &p->captureChannels[ch];

            LmBufInit(&cc->decodeBuf);
            cc->outFile = NULL;
            cc->writtenBytes = 0;
            cc->accumCount = 0;
            cc->accumLastUpdate = time(NULL);
            cc->pulseCountRate = 0;
            pthread_mutex_init(&cc->pulseCountRateMutex, NULL);
        }

        // Start the thread running.
        pthread_attr_t attrs;
        int rc = pthread_attr_init(&attrs);
        if (rc)
        {
            printf("can't initialise capture thread attributes - %s\n", strerror(errno));
            pthread_mutex_unlock(&p->captureMutex);
            p->state = CS_OFF;
            return;
        }

        p->state = newCaptureState;

        rc = pthread_create(&p->captureThread, &attrs, fxCaptureThread, (void *)p);
        if (rc)
        {
            printf("can't start capture thread - %s\n", strerror(errno));
            pthread_mutex_unlock(&p->captureMutex);
            p->state = CS_OFF;
            return;
        }

        p->captureThreadRunning = true;

        pthread_attr_destroy(&attrs);
        pthread_mutex_unlock(&p->captureMutex);
    }
    else
    {
        // Move between capture mode and count rate only mode.
        setState(p, newCaptureState);
    }
}


//
// External interface to stop capture. Stops the capture thread.
// Called from a client thread.
//

void fxCaptureStop(falconXport *p)
{
    // Is it already stopped?
    if (!isRunning(p))
        return;

    // Tell it to stop.
    requestStopCapturing(p);

    // Wait until it's stopped.
    pthread_join(p->captureThread, NULL);
}


