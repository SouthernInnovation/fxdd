#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <yaml.h>

#include "falconx.h"


// Path of the files.
#define ALS_ENVIRONMENT
#ifdef ALS_ENVIRONMENT
#define CONFIG_PREFIX "fxdd"
#else
#define CONFIG_PREFIX ".config/fxdd"
#endif

#define CONFIG_BASE_ENV "CONFIG_DIR"
#define CONFIG_PATH CONFIG_PREFIX "/fxdd_config.yaml"
#define RUN_NUMBER_PATH CONFIG_PREFIX "/run_number.yaml"


// Configuration key options.
enum ConfigKey
{
    KeyInvalid,
    KeyCaptureDirectory,
    KeyCaptureFilePattern,
    KeyMaximumSegmentSize,
    KeyPacketFilter,
    KeyPath1,
    KeyPath2,
    KeyPath3,
    KeyPath4
};


//
// Parse a comma separated list of packet filtering types into a bit set we can use.
//

static uint16_t parsePacketFilter(char *str)
{
    uint16_t filterBitset = 0;

    char *pos = str;
    char *nextPos;

    // Go through the string stepping by commas.
    for (pos = str; *pos != 0; pos = nextPos)
    {
        char *commaPos;

        // Eat leading whitespace.
        while (isspace(*pos))
        {
            pos++;
        }

        // Find the next comma.
        commaPos = index(pos, ',');
        if (commaPos)
        {
            // Step to the next character after the comma.
            *commaPos = 0;
            nextPos = commaPos + 1;
        }
        else
        {
            // No comma - skip to the end.
            nextPos = pos + strlen(pos);
        }

        // Add the next filter type into the bit set.
        if (strcmp(pos, "sync") == 0)
        {
            filterBitset |= 1 << LmPacketTypeSync;
        }
        else if (strcmp(pos, "streamAlign") == 0)
        {
            filterBitset |= 1 << LmPacketTypeStreamAlign;
        }
        else if (strcmp(pos, "pulse") == 0)
        {
            filterBitset |= 1 << LmPacketTypePulse;
        }
        else if (strcmp(pos, "gateState") == 0)
        {
            filterBitset |= 1 << LmPacketTypeGateState;
        }
        else if (strcmp(pos, "gatedStats") == 0)
        {
            filterBitset |= 1 << LmPacketTypeGatedStats;
        }
        else if (strcmp(pos, "spatialPosition") == 0)
        {
            filterBitset |= 1 << LmPacketTypeSpatialPosition;
        }
        else if (strcmp(pos, "spatialStats") == 0)
        {
            filterBitset |= 1 << LmPacketTypeSpatialStats;
        }
        else if (strcmp(pos, "periodicStats") == 0)
        {
            filterBitset |= 1 << LmPacketTypePeriodicStats;
        }
        else if (strcmp(pos, "internalBufferOverflow") == 0)
        {
            filterBitset |= 1 << LmPacketTypeInternalBufferOverflow;
        }
        else
        {
            printf("invalid filter type '%s' in config file\n", pos);
        }
    }

    return filterBitset;
}


//
// Gets the top-level directory used by configuration etc.
//

static void getTopDir(char *path)
{
    char *topDir;

    // What base directory are we working from?
    topDir = getenv(CONFIG_BASE_ENV);
    if (topDir == NULL)
    {
        topDir = getenv("HOME");
        if (topDir == NULL)
        {
            topDir = ".";
        }
    }

    strcpy(path, topDir);
}


//
// Read the config file in YAML format.
//
// In the captureDirectory or captureFilePattern:
//    $HOME is replaced by the home directory.
//    $RUN is replaced by the run number.
//    $SEGMENT is replaced by the segment number.
//    $WHEN is replaced by the run start date/time.
//    $DETECTOR is replaced by the detector id.
//
// maximumSegmentSize is the maximum size of a single capture file in MB.
//
// packetFilter is a comma-separated list of packet types to omit:
//    sync, pulse, gateState, gatedStats, spatialPosition, spatialStats, periodicStats, internalBufferOverflow
//
// Format
// -----------------------------------------------
// captureDirectory: $HOME/listmodedata
// captureFilePattern: $RUN/$RUN.$WHEN.$SEGMENT.$DETECTOR.silist
// maximumSegmentSize: 100     # in MB
// packetFilter:
//

void configRead(falconXport *p)
{
    char topDir[PATH_MAX+1];
    char fileName[PATH_MAX+1];
    FILE *inFile;
    yaml_parser_t parser;
    yaml_event_t  event;
    bool keyScan = true;
    enum ConfigKey key = KeyInvalid;

    // What base directory are we working from?
    getTopDir(topDir);

    // Assign some reasonable defaults in case they're not set in the file.
    memset(p->captureDir, 0, sizeof(p->captureDir));
    strncpy(p->captureDir, "$HOME/listmodedata", MAX_CAPTURE_DIR_LEN);
    memset(p->captureFilePattern, 0, sizeof(p->captureFilePattern));
    strncpy(p->captureFilePattern, "$RUN/$RUN.$WHEN.$DETECTOR.$SEGMENT.silist", MAX_CAPTURE_FILE_PATTERN_LEN);
    p->maxCaptureFileSize = 100;
    p->packetFilter = 0;

    // Open the config file.
    memset(fileName, 0, sizeof(fileName));
    snprintf(fileName, PATH_MAX, "%s/%s", topDir, CONFIG_PATH);

    inFile = fopen(fileName, "r");
    if (inFile == NULL)
    {
        printf("couldn't read config file %s - using defaults\n", fileName);
        return;
    }

    // Parse the YAML.
    if (!yaml_parser_initialize(&parser))
    {
        printf("couldn't initialise config file parser\n");
        fclose(inFile);
        return;
    }

    yaml_parser_set_input_file(&parser, inFile);

    do {
        // Get an event.
        if (!yaml_parser_parse(&parser, &event))
        {
            printf("can't parse YAML config file %s\n", fileName);
            yaml_parser_delete(&parser);
            fclose(inFile);
            return;
        }

        // Is it a scalar?
        if (event.type == YAML_SCALAR_EVENT)
        {
            char *v = (char *)event.data.scalar.value;

            if (keyScan)
            {
                // Check what kind of key this is.
                if (strcmp(v, "captureDirectory") == 0)
                {
                    key = KeyCaptureDirectory;
                }
                else if (strcmp(v, "captureFilePattern") == 0)
                {
                    key = KeyCaptureFilePattern;
                }
                else if (strcmp(v, "maximumSegmentSize") == 0)
                {
                    key = KeyMaximumSegmentSize;
                }
                else if (strcmp(v, "packetFilter") == 0)
                {
                    key = KeyPacketFilter;
                }
                else if (strcmp(v, "path1") == 0)
                {
                    key = KeyPath1;
                }
                else if (strcmp(v, "path2") == 0)
                {
                    key = KeyPath2;
                }
                else if (strcmp(v, "path3") == 0)
                {
                    key = KeyPath3;
                }
                else if (strcmp(v, "path4") == 0)
                {
                    key = KeyPath4;
                }
                else
                {
                    key = KeyInvalid;
                }

                keyScan = false;
            }
            else
            {
                // Assign a value depending on which key it was.
                switch (key)
                {
                case KeyInvalid:
                    break;

                case KeyCaptureDirectory:
                    strncpy(p->captureDir, v, MAX_CAPTURE_DIR_LEN);
                    break;

                case KeyCaptureFilePattern:
                    strncpy(p->captureFilePattern, v, MAX_CAPTURE_FILE_PATTERN_LEN);
                    break;

                case KeyMaximumSegmentSize:
                    p->maxCaptureFileSize = atoi(v);
                    break;

                case KeyPacketFilter:
                    p->packetFilter = parsePacketFilter(v);
                    break;

                case KeyPath1:
                    strncpy(p->path[0], v, MAX_PATH_SECTION_LEN);
                    break;

                case KeyPath2:
                    strncpy(p->path[1], v, MAX_PATH_SECTION_LEN);
                    break;

                case KeyPath3:
                    strncpy(p->path[2], v, MAX_PATH_SECTION_LEN);
                    break;

                case KeyPath4:
                    strncpy(p->path[3], v, MAX_PATH_SECTION_LEN);
                    break;
                }

                keyScan = true;
            }
        }

        if (event.type != YAML_STREAM_END_EVENT)
        {
            yaml_event_delete(&event);
        }

    } while(event.type != YAML_STREAM_END_EVENT);

    // Clean up.
    yaml_event_delete(&event);
    yaml_parser_delete(&parser);
    fclose(inFile);
}


//
// Reads the current run number from $HOME/.config/fxdd/run_number.yaml
//

int runNumberRead()
{
    char fileName[PATH_MAX+1];
    char topDir[PATH_MAX+1];
    FILE *inFile;
    yaml_parser_t parser;
    yaml_event_t  event;
    bool keyScan = true;
    bool correctKey = false;
    int  runNumber = 1;

    // What base directory are we working from?
    getTopDir(topDir);

    // Open the "run number" file.
    memset(fileName, 0, sizeof(fileName));
    snprintf(fileName, PATH_MAX, "%s/%s", topDir, RUN_NUMBER_PATH);

    inFile = fopen(fileName, "r");
    if (inFile == NULL)
        return runNumber;

    // Parse the YAML.
    if (!yaml_parser_initialize(&parser))
    {
        fclose(inFile);
        return runNumber;
    }

    yaml_parser_set_input_file(&parser, inFile);

    do {
        // Get an event.
        if (!yaml_parser_parse(&parser, &event))
        {
            yaml_parser_delete(&parser);
            fclose(inFile);
            return runNumber;
        }

        // Is it a scalar?
        if (event.type == YAML_SCALAR_EVENT)
        {
            if (keyScan)
            {
                // Check that this is the correct key.
                correctKey = strcmp((char *)event.data.scalar.value, "run_number") == 0;
                keyScan = false;
            }
            else
            {
                if (correctKey)
                {
                    // Assign the value.
                    runNumber = atoi((char *)event.data.scalar.value);
                    if (runNumber == 0)
                    {
                        runNumber = 1;
                    }
                }

                keyScan = true;
            }
        }

        if (event.type != YAML_STREAM_END_EVENT)
        {
            yaml_event_delete(&event);
        }

    } while(event.type != YAML_STREAM_END_EVENT);

    // Clean up.
    yaml_event_delete(&event);
    yaml_parser_delete(&parser);
    fclose(inFile);

    return runNumber;
}


//
// Increments the run number so next time we use a new one.
//

void runNumberWrite(int runNumber)
{
    char fileName[PATH_MAX+1];
    char topDir[PATH_MAX+1];
    FILE *outFile;

    // What base directory are we working from?
    getTopDir(topDir);

    // Open the "run number" file for writing.
    memset(fileName, 0, sizeof(fileName));
    snprintf(fileName, PATH_MAX, "%s/%s", topDir, RUN_NUMBER_PATH);

    if (!ensureDirectoryExists(fileName))
        return;

    outFile = fopen(fileName, "w");
    if (outFile == NULL)
        return;

    fprintf(outFile, "run_number: %d\n", runNumber);
    fclose(outFile);
}


//
// Increments the run number so next time we use a new one.
//

int runNumberReadAndIncrement()
{
    int runNumber = runNumberRead();

    runNumberWrite(runNumber + 1);

    return runNumber;
}


//
// Loads a complete configuration into a FalconX device including
// pulse calibration.
//

bool loadDeviceConfig(falconXport *p)
{
    char fileName[PATH_MAX+1];
    char topDir[PATH_MAX+1];

    // What base directory are we working from?
    getTopDir(topDir);

    // Open the "run number" file for writing.
    memset(fileName, 0, sizeof(fileName));
    snprintf(fileName, PATH_MAX, "%s/%s/%s.siprj", topDir, CONFIG_PREFIX, p->address);

    bool ok = SincProjectLoad(&p->sinc, fileName);
    if (ok)
    {
        printf("loaded device config file %s\n", fileName);
    }

    return ok;
}
