# fxdd

FalconX Data Daemon

An EPICS control interface for the FalconX series of pulse detection devices.

This interface allows list mode control and data capture via the EPICS "Experimental Physics and Industrial Control System".
